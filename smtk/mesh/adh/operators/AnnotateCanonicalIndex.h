//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef __smtk_mesh_adh_operators_AnnotateCanonicalIndex_h
#define __smtk_mesh_adh_operators_AnnotateCanonicalIndex_h

#include "smtk/mesh/adh/Exports.h"

#include "smtk/operation/XMLOperation.h"

namespace smtk
{
namespace mesh
{
namespace adh
{

/**\brief A class for annotating surface mesh elements with their canonical index

   This class constructs an integral cell field on the input surface mesh
   describing each element's "canonical index" as defined in Tautges, Timothy J.
   "Canonical numbering systems for finite‐element codes." International Journal
   for Numerical Methods in Biomedical Engineering 26.12 (2010): 1559-1572.
  */
class SMTKADHMESH_EXPORT AnnotateCanonicalIndex : public smtk::operation::XMLOperation
{
public:
  smtkTypeMacro(smtk::mesh::adh::AnnotateCanonicalIndex);
  smtkCreateMacro(AnnotateCanonicalIndex);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(smtk::operation::XMLOperation);

  bool ableToOperate() override;

protected:
  Result operateInternal() override;
  virtual const char* xmlDescription() const override;
};
}
}
}

#endif // __smtk_mesh_adh_operators_AnnotateCanonicalIndex_h
