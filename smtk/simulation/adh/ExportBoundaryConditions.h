//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#ifndef __smtk_simulation_adh_ExportBoundaryConditions_h
#define __smtk_simulation_adh_ExportBoundaryConditions_h

#include "smtk/simulation/adh/Exports.h"

#include "smtk/attribute/Attribute.h"

#include "smtk/mesh/core/Resource.h"
#include "smtk/mesh/core/Handle.h"

#include "smtk/model/Resource.h"

#include <pybind11/pybind11.h>

#include <set>
#include <string>

namespace smtk
{
namespace simulation
{
namespace adh
{

/// A functor for simultaneously generating an AdH .bc file and a .3dm geometry
/// file. The two files are generated at the same time because (a) the .bc file
/// is useless without its associated geometry file, and (b) the generation of
/// these two files in tandem ensures that cell ordering is consistent between
/// them.
///
/// This functor is designed to be called from python within the export process.
/// As such, it accepts an export "spec" python object commonly used during SMTK
/// exports to contain the state of the export.
class SMTKADHSIMULATION_EXPORT ExportBoundaryConditions
{
public:
  // Python entrypoint
  void operator()(pybind11::object spec);

  // C++ entrypoint
  void operator()(const std::string& geometryFileName,
                  const std::string& boundaryConditionFileName,
                  smtk::mesh::ResourcePtr& meshResource,
                  smtk::model::ResourcePtr& modelResource,
                  const std::string& modelPropertyName,
                  smtk::attribute::ResourcePtr& simAtts);

private:

  void writeGeometryPreamble(std::ofstream& geometryFile, std::size_t nCells, std::size_t nPoints);

  void writeBCPreamble(const std::string& boundaryConditionFileName);

  void writeNodeBoundaries(std::ofstream& bcFile,
                           smtk::mesh::ResourcePtr& meshResource,
                           const std::set<smtk::attribute::AttributePtr>& nodeBCs,
                           const std::unordered_map<smtk::mesh::Handle, std::size_t>& pointMap);

  void writeEdgeBoundaries(std::ofstream& bcFile,
                           smtk::mesh::ResourcePtr& meshResource,
                           const std::set<smtk::attribute::AttributePtr>& edgeBCs,
                           const std::unordered_map<smtk::mesh::Handle, std::size_t>& pointMap);
};

}
}
}

#endif
