set(unit_tests
  ExportBoundaryConditions.cxx
)

set(unit_tests_which_require_data
)

set(external_libs
  Boost::boost
  Boost::filesystem
  pybind11::embed
  ${PYTHON_LIBRARIES}
  )

unit_tests(
  LABEL "ADH Simulation"
  SOURCES ${unit_tests}
  SOURCES_REQUIRE_DATA ${unit_tests_which_require_data}
  LIBRARIES smtkCore smtkADHSimulation smtkADHMesh ${external_libs}
)
